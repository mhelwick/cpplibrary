#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#include "link.h"

template <class T>
class linkedList {
private:
	link<T> * head;
	link<T> * tail;
	int count;
public:
	linkedList() {
		this->head = NULL;
		this->tail = NULL;
		this->count = 0;
	}
	linkedList(link<T> * head) {
		this->head = head;
		this->tail = this->head;
		this->count = 1;
	}

	void setHead(link<T> * head) { this->head = head; }
	void setTail(link<T> * tail) { this->tail = tail; }
	void setCount(int count) { this->count = count; }

	link<T> * getHead() { return this->head; }
	link<T> * getTail() { return this->tail; }
	int getCount() { return this->count; }

	bool addLink(link<T> * newLink) {
		if(this->head == NULL) {
			this->head = newLink;
			this->head->setNext(NULL);
			this->tail = this->head;
		} else {
			this->tail->setNext(newLink);
			this->tail = newLink;
			this->tail->setNext(NULL);
		}
		return true;
	}

	link<T> * getLink(link<T> * searchLink) {
		link<T> * it = this->head;
		while(it != NULL) {
			if(it->getData() == searchLink->getData()) {
				return it;
			}
			it = it->getNext();
		}
		return NULL;
	}

	void printList() {	//only works on single-data link types
		link<T> * it = this->head;
		while(it != NULL) {
			std::cout << it->getData() << std::endl;
			it = it->getNext();
		}
	}

	bool insertAfter(link<T> * newLink, link<T> * insertionLink) {
		link<T> * it = this->head;
		while(it != NULL) {
			if(it->getData() == insertionLink->getData()) {
				link<T> * temp = it->getNext();
				it->setNext(newLink);
				it->getNext()->setNext(temp);
				return true;
			}
			it = it->getNext();
		}
		return false;
	}

	bool insertBefore(link<T> * newLink, link<T> * insertionLink) {
		link<T> * it = this->head;
		link<T> * itPrev = it;
		while(it != NULL) {
			if(it->getData() == insertionLink->getData()) {
				link<T> * temp = it;
				itPrev->setNext(newLink);
				itPrev->getNext()->setNext(temp);
				return true;
			}
			itPrev = it;
			it = it->getNext();
		}
		return false;
	}

	bool deleteLink(link<T> * searchLink) {
		link<T> * it = this->head;
		link<T> * itPrev = this->head;
		while(it != NULL) {
			if(it->getData() == searchLink->getData()) {
				itPrev->setNext(it->getNext());
				it->setPrev(NULL);
				it->setNext(NULL);
				delete it;
				return true;
			}
			itPrev = it;
			it = it->getNext();
		}
		return false;
	}

	void cleanup() {
		link<T> * it = this->head;
		link<T> * del = this->head;
		while(it != NULL) {
			del = it;
			it = it->getNext();
			delete del;
		}
	}
};

#endif
