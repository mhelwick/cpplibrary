#include "fileIO.h"

/*------------------------------------------------------------------------------
 * Name:        readStringFileByLine
 * Parameters:  input file name as string
 * Return Type: struct containg array and count
 * Purpose:     read a text file line by line and store in array of strings
 *----------------------------------------------------------------------------*/
stringReturn * readStringFileByLine(std::string fileName) {
	std::ifstream inputFile;
	inputFile.open(fileName.c_str());
	if(!inputFile.is_open()) {
		std::cerr << "Error: failed to open file: " << fileName << std::endl;
		return NULL;
	}

	std::string * lines;
	int arraySize = CHUNK_SIZE;
	int count = 0;
	lines = new (std::nothrow) std::string[arraySize];
	if(lines == nullptr) {
		std::cerr << "Error: memory allocation failed" << std::endl;
		return NULL;
	}

	std::string line;
	while(getline(inputFile, line)) {
		if(count == arraySize) {
			std::string * temp = new (std::nothrow) std::string[arraySize + CHUNK_SIZE];
			if(temp == nullptr) {
				std::cerr << "Error: memory allocation failed" << std::endl;
				return NULL;
			}

			arraySize += CHUNK_SIZE;
			for(int i = 0; i < count; i++) {
				temp[i] = lines[i];
			}

			delete lines;

			for(int i = 0; i < count; i++) {
				lines[i] = temp[i];
			}

			delete temp;
		}

		lines[count] = line;
		count++;
	}

	stringReturn * ret = new stringReturn;
	if(ret == NULL) {
		std::cerr << "Error: memory allocation failed" << std::endl;
		return NULL;
	}
	ret->lines = lines;
	ret->count = count;
	return ret;
}

/*------------------------------------------------------------------------------
 * Name:        readIntFileByLine
 * Parameters:  input file name as string
 * Return Type: struct containg array and count
 * Purpose:     read a text file line by line and store in array of strings
 *----------------------------------------------------------------------------*/
intReturn * readIntFileByLine(std::string fileName) {
	std::ifstream inputFile;
	inputFile.open(fileName.c_str());
	if(!inputFile.is_open()) {
		std::cerr << "Error: failed to open file: " << fileName << std::endl;
		return NULL;
	}

	int * lines;
	int arraySize = CHUNK_SIZE;
	int count = 0;
	lines = new (std::nothrow) int[arraySize];
	if(lines == nullptr) {
		std::cerr << "Error: memory allocation failed" << std::endl;
		return NULL;
	}

	std::string line;
	while(getline(inputFile, line)) {
		if(count == arraySize) {
			int * temp = new (std::nothrow) int[arraySize + CHUNK_SIZE];
			if(temp == nullptr) {
				std::cerr << "Error: memory allocation failed" << std::endl;
				return NULL;
			}

			arraySize += CHUNK_SIZE;
			for(int i = 0; i < count; i++) {
				temp[i] = lines[i];
			}

			delete lines;

			for(int i = 0; i < count; i++) {
				lines[i] = temp[i];
			}

			delete temp;
		}


		lines[count] = atoi(line.c_str());
		count++;
	}

	intReturn * ret = new intReturn;
	if(ret == NULL) {
		std::cerr << "Error: memory allocation failed" << std::endl;
		return NULL;
	}
	ret->lines = lines;
	ret->count = count;
	return ret;
}

/*------------------------------------------------------------------------------
 * Name:        writeStringToFile
 * Parameters:  array of strings from file read, string with name of file,
 * 				integer for number of lines
 * Return Type: bool to indicate success
 * Purpose:     write an array of strings to a file
 *----------------------------------------------------------------------------*/
bool writeStringToFile(std::string * lines, std::string fileName, int count) {
	std::ofstream outputFile;
	outputFile.open(fileName.c_str());
	if(!outputFile.is_open()) {
		std::cerr << "Error: failed to open file: " << fileName << std::endl;
		return false;
	}

	for(int i = 0; i < count; i++) {
		outputFile << lines[i] << std::endl;
	}

	return true;
}

/*------------------------------------------------------------------------------
 * Name:        writeIntToFile
 * Parameters:  array of ints from file read, string with name of file,
 * 				integer for number of lines
 * Return Type: bool to indicate success
 * Purpose:     write an array of ints to a file
 *----------------------------------------------------------------------------*/
bool writeIntToFile(int * lines, std::string fileName, int count) {
	std::ofstream outputFile;
	outputFile.open(fileName.c_str());
	if(!outputFile.is_open()) {
		std::cerr << "Error: failed to open file: " << fileName << std::endl;
		return false;
	}

	for(int i = 0; i < count; i++) {
		outputFile << lines[i] << std::endl;
	}

	return true;
}

/*------------------------------------------------------------------------------
 * Name:        splitStringsAtDelim
 * Parameters:  array of strings from file read, string delimiter, integer for
 *				number of lines
 * Return Type: bool to indicate success
 * Purpose:     write an array of ints to a file
 *----------------------------------------------------------------------------*/
stringReturn * splitStringsAtDelim(std::string * lines, std::string delim, int count) {
	std::string * temp;
	std::string * retLines = new std::string[LINE_MAX * count];
	size_t pos = 0;
	int j = 0, k = 0;
	std::string token;
	for(int i = 0; i < count; i++) {
		temp = new std::string[LINE_MAX];
		j = 0;
		while(((pos = lines[i].find(delim)) != std::string::npos) && j < LINE_MAX) {
			token = lines[i].substr(0,pos);
			lines[i].erase(0, pos + delim.length());
			temp[j] = token;
			j++;
		}
		token = lines[i].substr(0,lines[i].length());
		temp[j] = token;
		j++;
		for(int l = 0; l < j; l++) {
			retLines[k] = temp[l];
			k++;
		}
	}

	stringReturn * ret = new stringReturn;
	if(ret == NULL) {
		std::cerr << "Error: memory allocation failed" << std::endl;
		return NULL;
	}
	ret->lines = retLines;
	ret->count = k;
	return ret;
}
