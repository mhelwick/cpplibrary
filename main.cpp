#include "mhelwick.h"

int main() {
	stringReturn * stringRet = readStringFileByLine("test.txt");
	std::string * lines = stringRet->lines;
	int count = stringRet->count;

	for(int i = 0; i < count; i++) {
		std::cout << lines[i] << std::endl;
	}

	stringRet = splitStringsAtDelim(lines, " ", count);
	lines = stringRet->lines;
	count = stringRet->count;
	for(int i = 0; i < count; i++) {
		std::cout << lines[i] << std::endl;
	}

	linkedList<std::string> list;
	for(int i = 0; i < count; i++) {
		link<std::string> * newLink = new link<std::string>;
		newLink->setData(lines[i]);
		list.addLink(newLink);
	}

	list.printList();

	list.cleanup();

	return 0;
}
