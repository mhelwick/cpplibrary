#ifndef FILEIO_H
#define FILEIO_H

#include <iostream>
#include <string>
#include <fstream>
#include <new>
#include <cctype>

#define CHUNK_SIZE 10
#define LINE_MAX 100

typedef struct StringReturn {
	std::string * lines;
	int count;
} stringReturn;

typedef struct IntReturn {
	int * lines;
	int count;
} intReturn;

stringReturn * readStringFileByLine(std::string fileName);

intReturn * readIntFileByLine(std::string fileName);

bool writeStringToFile(std::string * lines, std::string fileName, int count);

bool writeIntToFile(int * lines, std::string fileName, int count);

stringReturn * splitStringsAtDelim(std::string * lines, std::string delim, int count);

#endif
