#ifndef LINK_H
#define LINK_H

template <class T>
class link {
private:
	T data;
	link<T> * next;
	link<T> * prev;
public:
	link() {
		this->next = NULL;
		this->prev = NULL;
	}
	link(T data) {
		this->data = data;
		this->next = NULL;
		this->prev = NULL;
	}
	link(T data, T * next) {
		this->data = data;
		this->next = next;
		this->prev = NULL;
	}
	link(T data, T * next, T * prev) {
		this->data = data;
		this->next = next;
		this->prev = prev;
	}

	void setData(T data) { this->data = data; }
	void setNext(link<T> * next) { this->next = next; }
	void setPrev(link<T> * prev) { this->prev = prev; }

	T getData() { return this->data; }
	link<T> * getNext() { return this->next; }
	link<T> * getPrev() { return this->prev; }
};

#endif
